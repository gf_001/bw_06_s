from django.urls import path
from app1 import views

urlpatterns = [
    path('index/',views.index,name='index'),
    path('add/',views.Add_new.as_view(),name='add'),
    path('new/<int:pk>/',views.New_info.as_view(),name='new'),
]
