from django.db import models
from django.contrib.auth.models import  AbstractUser
# Create your models here.

class User(AbstractUser):
    image = models.ImageField(verbose_name='图片',upload_to='user')
