from django.urls import path
from app1 import views
urlpatterns = [
    path('index/',views.index,name='index'),
    path('image/',views.Image.as_view(),name='image'),
]