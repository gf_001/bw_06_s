from django.shortcuts import render
from django.http import JsonResponse


# Create your views here.


def index(request):
    inof_list = ['图书列表', '作者列表', '出版社列表', '其他信息']
    book_info = [
        {'编号': 1, '名称': '西游记', '作者': '李峰', '价钱': 100},
        {'编号': 2, '名称': '三国演义', '作者': '崔亦鑫', '价钱': 90},
        {'编号': 3, '名称': '水浒传', '作者': '王兵', '价钱': 110},
        {'编号': 4, '名称': '红楼梦', '作者': '刘俊豪', '价钱': 120},
        {'编号': 5, '名称': '金瓶梅', '作者': '吴亚帅', '价钱': 119},
    ]
    # return render(request, 'show.html', context={'info_list': inof_list, 'book': book_info})
    return  render(request,'show.html',locals())
