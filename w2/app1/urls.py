from django.urls import path
from app1 import views
from django.contrib.auth.views import login_required

app_name = 'app1'
urlpatterns = [
    path('register/', views.Register.as_view(), name='register'),
    path('code/', views.code, name='code'),
    path('login_pwd/', views.Login_in.as_view(), name='login_pwd'),
    path('login_code/', views.Login_code.as_view(), name='login_code'),
    # 登录装饰器   不登录不允许访问
    path('user_show/', login_required(views.user_show.as_view()), name='user_show'),
    path('login_out/', views.login_out, name='login_out'),

]
