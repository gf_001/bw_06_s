from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.views import View
import re, string, random
from app1.models import User
from django.core.cache import cache


# Create your views here.


class Register(View):
    def get(self, request):
        return render(request, 'app1/register.html')

    def post(self, request):
        phone = request.POST.get('phone')
        pwd1 = request.POST.get('pwd1')
        pwd2 = request.POST.get('pwd2')
        if re.match(r'1[3,4,5,7,8]\d{9}', phone):
            if pwd1 == pwd2:
                User.objects.create_user(username=phone,
                                         password=pwd2,
                                         phone=phone)
                return JsonResponse({'msg': '注册成功'})
            else:
                return JsonResponse({'msg': '2次密码输入不一样'})
        else:
            return JsonResponse({'msg': '手机号不合法'})


def code(request):
    """
    获取验证码
    :param request:
    :return:
    """
    # 1 获取手机号
    # phone = request.POST.get('phone')
    # 2 生成验证码
    code = ''.join(random.choices(string.digits, k=6))
    # 3 调用 第三方接口  发生验证码到手机号
    # aa(phone,code)
    # 4. 判断缓存是否有验证码
    if cache.has_key('code'):
        return JsonResponse({'msg': '请勿频繁发送验证码'})
    # 5 把验证码添加到缓存中
    else:
        cache.set('code', code, 60)
        return JsonResponse({'msg': '发送成功', 'code': code})


from django.contrib.auth import logout, login, authenticate


class Login_in(View):
    def get(self, request):
        """
        跳转页面
        :param request:
        :return:
        """
        return render(request, 'app1/login.html')

    def post(self, request):
        # 1 获取前段的数据
        phone = request.POST.get('phone')
        pwd1 = request.POST.get('pwd1')
        # 2 数据验证
        u = authenticate(username=phone, password=pwd1)
        print(phone, pwd1, u)
        if u:
            # 添加session
            login(request, u)
            return redirect('app1:user_show')
        else:
            return JsonResponse({'msg': '账号或者密码错误', 'code': 201})


class Login_code(View):
    def get(self, request):
        """
        跳转页面
        :param request:
        :return:
        """
        return render(request, 'app1/login_code.html')

    def post(self, request):
        # 1 获取前段的数据
        phone = request.POST.get('phone')
        s_code = request.POST.get('code')
        # 2 获取换成的验证码
        code = cache.get('code')
        # 2 数据验证
        u = User.objects.filter(username=phone).first()
        if u:
            if s_code == code:
                # 添加session
                login(request, u)
                return redirect('app1:user_show')
            else:
                return JsonResponse({'msg': '验证码输入错误', 'code': 200})
        else:
            # 没有账号  重定向到 注册页面
            return redirect('app1:register')


class user_show(View):
    def get(self, request):
        # 1. 获取所有的用用户信息
        info = User.objects.all().order_by('last_login')
        # 2. 返回页面
        # return render(request, 'app1/user_show.html', locals())
        return render(request, 'app1/user_show.html', context={'inof':info})


def login_out(request):
    # 退出
    logout(request)
    # 调整到登陆页面
    return redirect('app1:login_pwd')
