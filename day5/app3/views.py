from django.shortcuts import render
from app1.models import Student
# Create your views here.

"""
flask 中页面的跳转 render_template()
"""
def index(request):
    """
    页面跳转
    :param request:
    :return:
    """
    # info = [
    #     {'name':'李峰','age':20},
    #     {'name':'曹蒙','age':19},
    #     {'name':'王兵','age':22},
    #     {'name':'吴亚帅','age':24},
    #     {'name':'刘俊豪','age':24}
    # ]
    # 1获取数据库的数据
    info = Student.objects.all()
    return render(request, 'app3/student_show1.html',context={'info':info})
