from django.shortcuts import render
from django.http import JsonResponse
from app1.models import *
from django.contrib.auth import login, logout, authenticate


# Create your views here

def index(request):
    return JsonResponse({'msg': '第一个gdfgfdagsafdfgsdf接口'})


def register_user(request):
    if request.method == 'GET':
        return JsonResponse({'msg': '不接受GET'})
    else:
        # 1 获取前端穿过来的数据
        name = request.POST.get('name')
        pwd = request.POST.get('pwd')

        # 2 数据效验
        u = User.objects.filter(username=name)
        if u:
            context = {'msg': '账号已存在'}
        # 3数据入库
        else:
            User.objects.create_user(username=name, password=pwd)
            context = {'msg': '注册成功'}
        # 4.数据返回
        return JsonResponse(context)


def login_in(request):
    if request.method == 'GET':
        return JsonResponse({'msg': '不接受GET'})
    else:
        # 1 获取前端穿过来的数据
        name = request.POST.get('name')
        pwd = request.POST.get('pwd')
        # 2 数据验证
        u = authenticate(username=name, password=pwd)
        if u:
            # 添加session
            login(request, u)
            context = {'msg': '登录成功', 'code': 200}
        else:
            context = {'msg': '账号/密码错误', 'code': 200}
        # 3 数据返回
        return JsonResponse(context)


def add_student(request):
    """
    添加学生
    :param request:
    :return:
    """
    if request.method == 'GET':
        return JsonResponse({'msg': '不接受GET'})
    else:
        # 1 获取前端穿过来的数据
        name = request.POST.get('name')
        age = request.POST.get('age')
        score = request.POST.get('score')
        grade_id = request.POST.get('grade_id')
        # 2. 数据效验
        try:
            Student.objects.get(name=name)
        except Student.DoesNotExist:
            # 3. 添加
            Student.objects.create(
                name=name, age=age,
                score=score, grade_id=grade_id
            )
            # s= Student(name=name,age=age,
            #     score=score,grade_id=grade_id)
            # s.save()
            context = {'msg': '添加成功', 'code': 200}
        else:
            context = {'msg': '重名', 'code': 200}
        # 数据返回
        return JsonResponse(context)

from django.db.models import F,Q,Sum,Min,Max,Avg
def select_student(request, pk):
    """
    查找学生
    :param request:
    :return:
    """
    # 1. 根据id获取数据 【queryset,queryset】
    # stu = Student.objects.filter(id=pk).first()
    # 根据分数
    stu = Student.objects.filter(score__gte=85,age__gt=22)
    # 最大的分数
    max_score = Student.objects.aggregate(Max('score'))

    print(max_score)
    # 2. 调整数据格式
    if stu:
        aa = []
        for x in stu:
            context = {
                'name': x.name,
                'age': x.age,
                'score': x.score,
                'grade': x.grade_id,
                'grade_name': x.grade.name,
            }
            aa.append(context)
    else:
        aa = [{'msg':'没有数据'}]
    # 3. 数据返回
    return JsonResponse(aa,safe=False)


def update_student(request,pk):
    """
    修改
    :param request:
    :return:
    """
    """
    #1 元数据的获取
    s = Student.objects.filter(id=pk).first()
    #2. 获取需要更改的数
    age = request.POST.get('age')
    score = request.POST.get('score')
    # 3 修改
    s.age=age
    s.score= score
    # 4 保存
    s.save()
    """

    # 1. 获取需要更改的数
    age = request.POST.get('age')
    score = request.POST.get('score')
    # 2 元数据的获取
    Student.objects.filter(id=pk).update(age=age,score=score)
    #5 数据返回
    return  JsonResponse({'msg':'更新成功'})



def delete_student(request,pk):
    """
    删除
    :param request:
    :return:
    """
    # 2 元数据的获取
    Student.objects.filter(id=pk).delete()
    # 5 数据返回
    return JsonResponse({'msg': '删除成功'})
