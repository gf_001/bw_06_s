from django.db import models
from django.contrib.auth.models import AbstractUser
# Create your models here.

class User(AbstractUser):
    phone = models.CharField(verbose_name='手机号',max_length=11)
    user_image = models.ImageField(verbose_name='用户图像',upload_to='user')

class Grade(models.Model):
    name = models.CharField(verbose_name='班级名称',max_length=10)

class Student(models.Model):
    name = models.CharField(verbose_name='学生姓名',max_length=10)
    age= models.IntegerField(verbose_name='年龄')
    score= models.IntegerField(verbose_name='分数')
    grade = models.ForeignKey(to='Grade',to_field='id',on_delete=models.CASCADE)
