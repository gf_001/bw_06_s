from django.urls import path
from app1 import views
urlpatterns = [
    path('index/',views.index,name='index'),
    path('register_user/',views.register_user,name='register_user'),
    path('login_in/',views.login_in,name='login_in'),
    path('add_student/',views.add_student,name='add_student'),
    path('select_student/<int:pk>/',views.select_student,name='select_student'),
    path('update_student/<int:pk>/',views.update_student,name='update_student'),
    path('delete_student/<int:pk>/',views.delete_student,name='delete_student'),
]